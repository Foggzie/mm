﻿using UnityEngine;

public class YeetSpawner : MonoBehaviour
{
    public GameObject Prefab;

    public Camera Camera;

    public void Spawn()
    {
        var ray = Camera.ViewportPointToRay(new Vector3(Random.Range(0.2f, 0.8f), 0f, 0f));
        var plane = new Plane(new Vector3(0,0,1), 0f);
        if (plane.Raycast(ray, out var hit))
        {
            Instantiate(Prefab, ray.GetPoint(hit), Quaternion.identity);
        }
    }
}

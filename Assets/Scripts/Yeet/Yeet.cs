﻿using System.Collections;
using UnityEngine;

public class Yeet : MonoBehaviour
{
    public Rigidbody2D body;

    public float firstWait;
    public float force;
    public float lifetime;

    private IEnumerator Start()
    {
        body.gravityScale = 0f;
        body.velocity = Vector2.up * Random.Range(0.2f, 0.3f);

        yield return new WaitForSeconds(firstWait);
        body.gravityScale = 0.1f;

        var forceDirection = new Vector2(Random.Range(-1f, 1f), 1f);
        var forcePosition = new Vector2(Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        body.AddForceAtPosition(forceDirection * force, forcePosition, ForceMode2D.Impulse);

        yield return new WaitForSeconds(lifetime);

        Destroy(gameObject);
    }
}
